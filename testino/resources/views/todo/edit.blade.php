@extends('todo.layouts.app')


@section('content')

<div class="box"></div>

<div class="container">
    <form>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th>項目</th>
                    <td><input type="text" name="title" value='{{$todo->title}}'></td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <th>内容</th>
                    <td><textarea name="body">{{$todo->body}}</textarea></td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <th>金額</th>
                    <td><input type="text" name="price" value='{{$todo->price}}'>円</td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <th>期限</th>
                    <td><input type="date" name="date" value="{{$todo->limit}}"></td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <th>削除</th>
                    <td><a href="/todo/{{$todo->id}}" class="btn btn-danger">削除</a></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
@endsection