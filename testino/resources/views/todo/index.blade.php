@extends('todo.layouts.app')


@section('content')

<div class="box"></div>

<div class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>期限</th>
                <th>項目</th>
                <th>内容</th>
                <th>金額</th>
                <th>グレード</th>
                <th>状況</th>
                <th>ボタン</th>
            </tr>
        </thead>
        @foreach($todo as $td)
        <tbody>
            <tr>
                @if(($td->limit) >= date('Y-m-d') )
                <th scope="row">{{$td->limit}}</th>
                @else
                <th scope="row" style="color:red">{{$td->limit}}</th>
                @endif
                <td>{{$td->title}}</td>
                <td>{{$td->body}}</td>
                <td>{{$td->price}}円</td>a
                <td>{{$td->period}}</td>
                <td>
                    <form action="/todo/{{$td->id}}/edit" method="get">
                    <input type="submit" class="btn btn-primary" value="詳細">
                    </form>
                </td>
                <td>
                    <form action="/todo/{{$td->id}}/paied" method="get">
                        <input type="submit" value="OK" class="btn btn-primary">
                    </form>
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
</div>
@endsection