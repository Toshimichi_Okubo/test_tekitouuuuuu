<?php

use Illuminate\Database\Seeder;

class TodoListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todo_lists')->insert([
            'title' => 'ガス代払う',
            'body' => '11月分2000円', 
            'limit' => '2019-1-15', 
            'period'=> 1 ,
        ]);
        
        
    }
}
